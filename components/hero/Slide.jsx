import React, { Component, useState, useEffect } from "react";
import classnames from "classnames/bind";

class Slide extends React.Component {
  state = {
    contentIn: false,
  };

  inInterval = null;
  outInterval = null;
  active = false;

  componentDidUpdate = (prevProps) => {
    if (this.props.active === true && this.active === false) {
      this.active = true;

      this.inInterval = setTimeout(() => {
        this.setState({ contentIn: true });
      }, 500);

      this.outInterval = setTimeout(() => {
        this.setState({ contentIn: false });
      }, this.props.duration - 500);
    } else if (this.props.active === false && this.active === true) {
      clearInterval(this.inInterval);
      clearInterval(this.outInterval);
      this.active = false;
      this.setState({ contentIn: false });
    }
  };
  render() {
    return (
      <div
        className={classnames({
          slide: true,
          ["slide-" + this.props.id]: true,
        })}
      >
        <div className="slide__progress">
          <span></span>
        </div>

        <div
          className={classnames({
            container: true,
            slide__content: true,
            "slide__content--in": this.state.contentIn,
          })}
          onMouseDown={(e) => {
            e.stopPropagation();
          }}
          onClick={(e) => {
            e.stopPropagation();
          }}
          onMouseEnter={(e) => {
            this.props.hideMouse();
          }}
          onMouseLeave={(e) => {
            this.props.showMouse();
          }}
        >
          <div className="slide__content__left">
            <h2>{this.props.title}</h2>
          </div>
          <div className="slide__content__right">
            <h3>{this.props.subtitle}</h3>
            <p>{this.props.text}</p>
            {this.props.linkText && (
              <a href={this.props.link} target="_blank" rel="noreferrer">
                <span className="text">{this.props.linkText}</span>
                <span className="arrow"></span>
              </a>
            )}
          </div>
        </div>
        <div
          className="slide__image"
          style={{
            backgroundImage: `url('${this.props.image}')`,
          }}
        />
      </div>
    );
  }
}

export default Slide;
