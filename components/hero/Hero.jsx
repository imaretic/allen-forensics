import React, { Component } from "react";
import { Transition, CSSTransition } from "react-transition-group";
import classnames from "classnames/bind";

import Slide from "./Slide";

const slideDuration = 15000;
const fadeDuration = 600;

class Hero extends Component {
  state = {
    slide: 0,
    contentIn: false,
    cursorX: 0,
    cursorY: 0,
    mouseDown: false,
    mouseEntered: false,
    idle: false,
  };

  interval = null;

  componentDidMount = () => {
    this.startLoop();
  };

  startLoop = () => {
    this.interval = setInterval(() => {
      this.nextSlide();
    }, slideDuration);
  };

  mouseMoved = (e) => {
    this.setIdleInterval();
    this.setState({ cursorX: e.clientX, cursorY: e.clientY });
  };

  mouseDown = (e) => {
    this.setState({ idle: false, mouseDown: true });
  };

  mouseClicked = (e) => {
    this.setIdleInterval();
    clearInterval(this.interval);
    this.setState({ mouseDown: false });
    this.nextSlide();
    this.startLoop();
  };

  mouseTimeoutInterval = undefined;

  showMouse = (e) => {
    this.setState({ mouseEntered: true });
    this.setIdleInterval();
  };

  setIdleInterval = () => {
    this.setState({ idle: false });
    clearInterval(this.mouseTimeoutInterval);
    this.mouseTimeoutInterval = setTimeout(() => {
      this.setState({ idle: true });
    }, 1000);
  };

  hideMouse = () => {
    clearInterval(this.mouseTimeoutInterval);
    this.setState({ mouseEntered: false });
  };

  nextSlide = (e) => {
    this.setState((prevState) => {
      return { slide: (prevState.slide + 1) % this.slides.length };
    });
  };

  slides = [
    <Slide
      key={1}
      id={1}
      title="We help identify your most critical digital business assets"
      subtitle="Cyber Risk Advisory "
      text="Cybersecurity governance starts at the top, with the board of directors’ oversight and executives setting management direction. Many companies still view cyber risks primarily as an IT issue. Practically, cyber risk Is really business risk. "
      duration={slideDuration}
      showMouse={this.showMouse}
      hideMouse={this.hideMouse}
      image="/img/photos/photo_1.jpg"
    />,
    <Slide
      key={2}
      id={2}
      title="We help people with forensic accounting"
      subtitle="Forensic Accounting and Investigations"
      text="Our professionals are fully trained and experienced in all aspects of our practice including litigation support, business interruption claims, lost profits claims, economic damages, business disputes and fraud investigation cases."
      duration={slideDuration}
      showMouse={this.showMouse}
      hideMouse={this.hideMouse}
      image="/img/photos/photo_4.jpg"
    />,
    <Slide
      key={3}
      id={3}
      title="We defend your organization by preserving the integrity of evidence"
      subtitle="Digital Forensics"
      text="We help to quickly locate pertinent data from the devices you see every day, from iOS to Windows machines for various internal investigations including personnel, Human Resources, Intellectual Property Theft, and workplace harassment."
      duration={slideDuration}
      showMouse={this.showMouse}
      hideMouse={this.hideMouse}
      image="/img/photos/photo_2.jpg"
    />,
    <Slide
      key={4}
      id={4}
      title="We help you legally reduce your tax liability"
      subtitle="Tax Services"
      text="Individual and Small business tax return preparation and filing is a core service offered to our clients. Whether your company is a C corporation, S Corp, Limited Liability Company, Partnership or Sole Proprietor a properly prepared tax return can be filed to minimize the tax owed."
      linkText="Where is my refund?"
      link="https://www.irs.gov/refunds"
      duration={slideDuration}
      showMouse={this.showMouse}
      hideMouse={this.hideMouse}
      image="/img/photos/photo_3.jpg"
    />,
  ];

  render() {
    return (
      <section className="hero">
        <div
          className="slider-wrapper"
          onClick={() => {
            this.mouseClicked();
          }}
          onMouseMove={this.mouseMoved}
          onMouseEnter={this.showMouse}
          onMouseLeave={this.hideMouse}
          onMouseDown={this.mouseDown}
        >
          <div className="bullets-wrapper">
            <div className="container">
              <div
                className="bullets"
                onMouseEnter={this.hideMouse}
                onMouseLeave={this.showMouse}
                onMouseDown={(e) => {
                  e.stopPropagation();
                }}
              >
                {this.slides.map((slide, index) => {
                  return (
                    <div
                      key={index}
                      className={classnames({
                        bullets__bullet: true,
                        "bullets__bullet--active": index === this.state.slide,
                      })}
                      onMouseDown={() => {}}
                      onClick={(e) => {
                        e.stopPropagation();
                        clearInterval(this.interval);
                        this.setState({ slide: index });
                        this.startLoop();
                      }}
                    />
                  );
                })}
              </div>
            </div>
          </div>
          <CSSTransition
            in={this.state.mouseEntered}
            timeout={1000}
            classNames="cursor-transition"
            unmountOnExit
          >
            <div
              className={classnames({
                cursor: true,
                "cursor--active": this.state.mouseDown,
                "cursor--idle": this.state.idle,
              })}
              style={{
                top: this.state.cursorY + "px",
                left: this.state.cursorX + "px",
              }}
            >
              <div className="cursor__image"></div>
              <div className="cursor__bg"></div>
            </div>
          </CSSTransition>
          {this.slides.map((TheSlide, index) => (
            <CSSTransition
              key={index}
              appear={true}
              in={this.state.slide === index}
              timeout={fadeDuration}
              classNames="slide-transition"
            >
              {React.cloneElement(TheSlide, {
                active: this.state.slide === index,
              })}
            </CSSTransition>
          ))}
        </div>
      </section>
    );
  }
}

export default Hero;
