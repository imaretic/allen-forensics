import React, { useState, useEffect } from "react";
import { Btn } from "./Btn";

const Cookies = (props) => {
  const [shown, setShown] = useState(true);

  useEffect(() => {
    const shown = localStorage.getItem("cookie-shown");
    if (shown) setShown(true);
    else setShown(false);
  }, []);

  const handleClick = (e) => {
    localStorage.setItem("cookie-shown", true);
    setShown(true);
  };

  if (!shown)
    return (
      <div className="cookies">
        <p>
          By continuing to browse or by clicking “Accept”, you agree to the
          storing of cookies on your device to enhance your site experience.
        </p>
        <Btn
          label="Accept and close"
          nowrap={true}
          small={true}
          onClick={handleClick}
        />
      </div>
    );

  return <div></div>;
};

export default Cookies;
