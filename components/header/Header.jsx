import React, { useState, useEffect } from "react";
import classnames from "classnames/bind";

const SCROLL_OFFSET = 50;

const Header = () => {
  const [scroll, setScroll] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  const handleScroll = () => {
    if (window.scrollY > SCROLL_OFFSET) {
      setScroll(true);
    } else {
      setScroll(false);
    }
  };

  return (
    <header className={classnames({ scroll: scroll })}>
      <div className="container">
        <div className="logo">
          <img
            src="/img/logo_icon.svg"
            alt="Allen Forensics Logo Symbol"
            className="logo__symbol"
          />
          <img
            src="/img/logo_name.svg"
            alt="Allen Forensics Logo Text"
            className="logo__text"
          />
        </div>
        <div className="cta">
          <a href="#contact" className="btn btn--default nowrap">
            Contact Us
          </a>
        </div>
      </div>
    </header>
  );
};

export default Header;
