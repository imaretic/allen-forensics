import React, { Component } from "react";
import NewsletterForm from "./NewsletterForm";

const Whitepaper = () => {
  return (
    <section className="whitepaper">
      <div className="container">
        <div className="whitepaper__left">
          <img
            src="/img/photos/whitepaper.png"
            alt="Third Party Fraud Whitepaper Cover"
          />
        </div>
        <div className="whitepaper__right">
          <h3>Whitepaper</h3>
          <h4>Download FREE whitepaper about Third Party Fraud</h4>
          <div className="whitepaper__form">
            <NewsletterForm />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Whitepaper;
