import React from "react";
import MailchimpSubscribe from "react-mailchimp-subscribe";
import { Btn } from "../Btn";

class CustomForm extends React.Component {
  state = {
    empty: false,
  };

  submit(e) {
    e.preventDefault();
    const email = e.target.email.value;
    if (this.validateEmail(email)) {
      this.setState({ empty: false });
      this.props.onValidated({
        EMAIL: email,
      });
    } else {
      this.setState({ empty: true });
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  render() {
    return (
      <div className="newsletter-form-wrapper">
        {this.props.status !== "success" && (
          <form onSubmit={this.submit.bind(this)} className="newsletter-form">
            <input
              type="email"
              aria-label="Email"
              name="email"
              id="email"
              className="newsletter-form__email"
              placeholder="Your email"
              disabled={this.props.status === "sending"}
            />
            <Btn
              label="Download"
              type="submit"
              wide="true"
              dark="true"
              disabled={this.props.status === "sending"}
            />
          </form>
        )}

        {this.props.status === "sending" && (
          <div className="newsletter-form-loading">
            <img src="/img/newsletter_loader.svg" />
          </div>
        )}
        {this.props.status === "error" && (
          <div
            className="newsletter-form-message newsletter-form-message--error"
            dangerouslySetInnerHTML={{ __html: this.props.message }}
          />
        )}
        {this.state.empty && (
          <div
            className="newsletter-form-message newsletter-form-message--error"
            dangerouslySetInnerHTML={{
              __html: "Please enter e-mail address.",
            }}
          />
        )}
        {this.props.status === "success" && (
          <div
            className="newsletter-form-message newsletter-form-message--success"
            dangerouslySetInnerHTML={{ __html: this.props.message }}
          />
        )}
      </div>
    );
  }
}

class NewsletterForm extends React.Component {
  render() {
    const url =
      "https://allenforensics.us20.list-manage.com/subscribe/post?u=8629e375abfe668c4fda96e41&amp;id=52da0ef000";
    return (
      <MailchimpSubscribe
        url={url}
        render={({ subscribe, status, message }) => (
          <CustomForm
            status={status}
            message={message}
            onValidated={(formData) => subscribe(formData)}
          />
        )}
      />
    );
  }
}

export default NewsletterForm;
