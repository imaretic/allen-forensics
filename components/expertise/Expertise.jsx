import React, { Component } from "react";

const Expertise = () => {
  return (
    <section className="expertise">
      <div className="container">
        <div className="expertise__left">
          <h3 className="black">Our expertise</h3>
          <p>
            We specialize in Investigating fraud, corruption, conducting
            interviews, analyzing and reconstructing digital information and
            financial records, assessing fraud vulnerability, analyzing
            embezzlement allegations and developing strategic cybersecurity
            plan. Financial institutions, law firms, government agencies and
            others regularly retain our firm to provide investigative support in
            adversarial matters and expert testimony at trial or in depositions.{" "}
          </p>
        </div>
        <div className="expertise__right">
          <p>
            We have extensive experience assisting counsel and their clients in
            all phases of litigation, from discovery through trial, as well as
            alternative dispute resolution proceedings.
          </p>
        </div>
      </div>
    </section>
  );
};

export default Expertise;
