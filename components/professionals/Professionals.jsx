import React, { Component } from "react";
import Person from "./Person";

const Professionals = () => {
  return (
    <section className="professionals">
      <div className="container">
        <h3 className="black">Our professionals</h3>
        <div className="professionals__group">
          <Person
            name="Ben Allen"
            role="Managing Partner"
            bio="Ben has 10 years combined experience in Forensic accounting and Cyber Risk advisory. He has Fortune 500 service experience having worked for major corporations like Capital One Financial, Standard Chartered Bank, MoneyGram, Banco Popular and others. He has investigated over 1,000 cases of financial crime. He works with counsel and their clients on complex accounting, auditing and financial issues throughout the litigation process."
            img="/img/photos/ben.jpg"
          />
          <Person
            name="Jesseca Jamison"
            role="Partner"
            bio="Jess is a US Army Veteran with wide experience in Corporate Communications, Internal investigations, Account Take Over (ATO). She is also skilled in Human Source Intelligence, Surveillance, and identity theft remediation. Jess is particularly passionate about leadership, market penetration and organizational growth."
            img="/img/photos/jess.jpg"
          />
          <Person
            name="Jacob Bryant"
            role="Partner"
            bio="Jacob is a Licensed Private Investigator and has a comprehensive and authoritative knowledge of computer science, an expert in crypto currency mining, storage, distribution and application of the dark web with advanced knowledge of cryptography and secure communications. He has a long career in defense industry with experience adapting new technologies to solve problems and an extensive experience with cyberthreats from a lone wolf to a nation state."
            img="/img/photos/jacob.jpg"
          />
        </div>
      </div>
    </section>
  );
};

export default Professionals;
