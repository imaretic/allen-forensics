import React, { Component, useState } from "react";
import classnames from "classnames/bind";

const Person = (props) => {
  const [open, setOpen] = useState(false);

  return (
    <div
      className={classnames({ person: true, "person--open": open })}
      onClick={(e) => !open && setOpen(true)}
    >
      <img className="person__photo" src={props.img} alt={props.name} />
      <p className="person__bio">{props.bio}</p>
      <div className="person__shade"></div>
      <div className="person__meta">
        <div className="person__meta__left">
          <h4 className="nowrap">{props.name}</h4>
          <p>{props.role}</p>
        </div>
        <div className="person__meta__right">
          <button
            aria-label="Show biography"
            type="button"
            onClick={(e) => open && setOpen(false)}
          >
            <div className="arrow">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20.59"
                height="11.709"
                viewBox="0 0 20.59 11.709"
              >
                <g id="arrow_expand" transform="translate(-1446.541 -853.64)">
                  <path
                    id="arrow_core"
                    data-name="Path 1"
                    d="M222.856,776.516l8.881,8.881-8.881,8.881"
                    transform="translate(671.439 1086.792) rotate(-90)"
                    fill="none"
                    stroke="#fff"
                    strokeLinecap="round"
                    strokeWidth="2"
                  />
                </g>
              </svg>
            </div>
            <div className="x">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="18.454"
                height="18.454"
                viewBox="0 0 18.454 18.454"
              >
                <g
                  id="Group_10"
                  data-name="Group 10"
                  transform="translate(-1706.346 -2986.96)"
                >
                  <path
                    id="Path_8"
                    data-name="Path 8"
                    d="M1609.76,2997l15.626-15.626"
                    transform="translate(98 7)"
                    fill="none"
                    stroke="#000"
                    strokeLinecap="round"
                    strokeWidth="2"
                  />
                  <path
                    id="Path_9"
                    data-name="Path 9"
                    d="M1609.76,2997l15.626-15.626"
                    transform="translate(-1273.614 4613.76) rotate(-90)"
                    fill="none"
                    stroke="#000"
                    strokeLinecap="round"
                    strokeWidth="2"
                  />
                </g>
              </svg>
            </div>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Person;
