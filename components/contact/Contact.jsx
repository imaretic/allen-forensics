import React, { Component, useState } from "react";
import {
  validateName,
  validateEmail,
  validateMessage,
} from "../../utils/validator";
import classnames from "classnames/bind";

import { Btn } from "../Btn";

const Contact = () => {
  const [submitted, setSubmitted] = useState(false);
  const [loading, setLoading] = useState(false);
  const [formMessage, setFormMessage] = useState("");
  const [formMessageType, setFormMessageType] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    const name = e.target.name.value;
    const email = e.target.email.value;
    const message = e.target.message.value;
    setSubmitted(true);

    // Check name and message
    if (!validateName(name) || !validateMessage(message)) {
      setFormMessage("All fields are mandatory!");
      setFormMessageType("error");
      return;
    }

    // Check email
    if (!validateEmail(email)) {
      setFormMessage("Please enter correct email address!");
      setFormMessageType("error");
      return;
    }

    setLoading(true);

    fetch("/api/contact", {
      method: "post",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name, email, message }),
    }).then((res) => {
      setLoading(false);
      if (res.status === 200) {
        setFormMessage(
          "Thanks for the message! We’ll get back to you shortly."
        );
        setFormMessageType("success");
        setSubmitted(false);
        setName("");
        setEmail("");
        setMessage("");
      } else {
        setFormMessage(
          "There was an error. Please contact us via social networks or try again later."
        );
        setFormMessageType("error");
      }
    });
  };

  return (
    <section className="contact section--white">
      <div className="container">
        {formMessage && (
          <div className="contact-message">
            {formMessageType === "success" ? (
              <img src="/img/success_icon.svg" alt="Success" />
            ) : (
              <img src="/img/error_icon.svg" alt="Error" />
            )}
            <p>{formMessage}</p>
          </div>
        )}
        <div className="contact-wrapper">
          <h3 className="nowrap">Get in touch</h3>
          <form className="contact__form" onSubmit={handleSubmit}>
            <div className="contact__form__fields">
              <div className="contact__form__fields__top">
                <input
                  type="text"
                  name="name"
                  aria-label="Name"
                  placeholder="Name"
                  className={classnames({
                    error: !validateName(name) && submitted,
                  })}
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  disabled={loading}
                />
                <input
                  type="email"
                  name="email"
                  aria-label="Email"
                  placeholder="E-mail address"
                  className={classnames({
                    error: !validateEmail(email) && submitted,
                  })}
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  disabled={loading}
                />
              </div>
              <textarea
                name="message"
                aria-label="Message"
                placeholder="Message"
                className={classnames({
                  error: !validateMessage(message) && submitted,
                })}
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                disabled={loading}
              />
            </div>
            <Btn label="Submit" type="submit" loading={loading} wide={true} />
          </form>
        </div>
      </div>
    </section>
  );
};

export default Contact;
