import React from "react";
import classnames from "classnames/bind";

export const Btn = (props) => {
  return (
    <button
      type={props.type}
      className={classnames({
        btn: true,
        "btn--default": true,
        "btn--wide": props.wide,
        "btn--small": props.small,
        "btn--dark": props.dark,
        nowrap: props.nowrap,
        loading: props.loading,
      })}
      disabled={props.loading}
      onClick={props.onClick}
    >
      {props.label}
      <img className="loader" src="/img/loader_white.svg" alt="Loading..." />
    </button>
  );
};
