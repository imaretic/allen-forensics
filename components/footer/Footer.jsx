import React, { Component } from "react";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

library.add(fab);

const Footer = () => {
  return (
    <footer className="footer section--white">
      <div className="container">
        <div className="footer__logo">
          <img src="/img/logo_footer.svg" alt="Allen Forensics Logo" />
        </div>
        <div className="footer__info">
          <div className="footer__info__copyright">
            Copyright @ 2020 Allen Forensics
          </div>
          <div className="footer__info__legal">
            Texas License: #A08370801
            <br />
            Dallas.Texas.75206
          </div>
        </div>
        <div className="footer__social">
          <a
            href="https://www.youtube.com/channel/UCjEs_yHgNIIJCHOJHZzTtAA/"
            className="youtube"
            target="_blank"
            rel="noreferrer"
          >
            <FontAwesomeIcon icon={["fab", "youtube"]} />
            <span className="sr-only">YouTube Channel</span>
          </a>
          <a
            href="https://www.facebook.com/allenforensics"
            className="facebook"
            target="_blank"
            rel="noreferrer"
          >
            <FontAwesomeIcon icon={["fab", "facebook-f"]} />
            <span className="sr-only">Facebook Profile</span>
          </a>
          <a
            href="https://www.instagram.com/allenforensics"
            className="instagram"
            target="_blank"
            rel="noreferrer"
          >
            <FontAwesomeIcon icon={["fab", "instagram"]} />
            <span className="sr-only">Instagram Profile</span>
          </a>
          <a
            href="https://twitter.com/allenforensics"
            className="twitter"
            target="_blank"
            rel="noreferrer"
          >
            <FontAwesomeIcon icon={["fab", "twitter"]} />
            <span className="sr-only">Twitter Profile</span>
          </a>
        </div>
        <div className="footer__credits nowrap">
          Web by <a href="https://winner.agency/">Winner</a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
