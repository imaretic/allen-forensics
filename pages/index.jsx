import Head from "next/head";
import ScrollableAnchor from "react-scrollable-anchor";
import Header from "../components/header/Header";
import Hero from "../components/hero/Hero";
import Expertise from "../components/expertise/Expertise";
import Professionals from "../components/professionals/Professionals";
import Whitepaper from "../components/whitepaper/Whitepaper";
import Footer from "../components/footer/Footer";
import Contact from "../components/contact/Contact";

import { configureAnchors } from "react-scrollable-anchor";
import Cookies from "../components/Cookies";

// Offset all anchors by -60 to account for a fixed header
// and scroll more quickly than the default 400ms
configureAnchors({ offset: 0, scrollDuration: 500 });

export default function Home() {
  const title =
    "Allen Forensics | Cyber Risk Advisory, Digital & Financial Forensics";
  const description =
    "Allen Forensics mission is to promote digital stewardship by design to help organizations establish a dynamic cybersecurity posture.";

  return (
    <div className="homepage">
      <Head>
        <title>{title}</title>

        {/* General tags */}
        <meta name="description" content={description} />
        <meta
          name="keywords"
          content="allen forensics, forensic accounting, tax planning, tax implementation, payroll services, accounting issues, financial fraud investigation, financial services, digital forensics, cyber risk advisory, third party fraud, cyber security"
        />

        {/* Twitter */}
        <meta name="twitter:card" content={description} key="twcard" />
        <meta name="twitter:creator" content="Allen Forensics" key="twhandle" />
        <meta name="twitter:site" content="allenforensics" key="twsite" />

        {/* Open Graph */}
        <meta
          property="og:image"
          content="/img/photos/AF_thumbnail_1200x630px.jpg"
          key="ogimage"
        />
        <meta
          property="og:site_name"
          content="Allen Forensics"
          key="ogsitename"
        />
        <meta property="og:title" content={title} key="ogtitle" />
        <meta property="og:description" content={description} key="ogdesc" />

        {/* Favicons */}
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/img/favicons/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/img/favicons/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/img/favicons/favicon-16x16.png"
        />
        <link rel="manifest" href="/img/favicons/site.webmanifest" />
        <link
          rel="mask-icon"
          href="/img/favicons/safari-pinned-tab.svg"
          color="#5bbad5"
        />
        <link rel="shortcut icon" href="/img/favicons/favicon.ico" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta
          name="msapplication-config"
          content="/img/favicons/browserconfig.xml"
        />
        <meta name="theme-color" content="#ffffff" />
        <link
          href="https://fonts.googleapis.com/css2?family=Archivo+Black&display=swap"
          rel="stylesheet"
          media="print"
          onLoad="this.media='all'"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap"
          rel="stylesheet"
          media="print"
          onLoad="this.media='all'"
        />
      </Head>

      <Cookies />
      <Header />
      <Hero />
      <Expertise />
      <Professionals />
      <Whitepaper />
      <ScrollableAnchor id={"contact"}>
        <div>
          <Contact />
        </div>
      </ScrollableAnchor>
      <Footer />
    </div>
  );
}
