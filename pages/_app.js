import App from "next/app";
import Router from "next/router";
import React, { useEffect } from "react";
import { useAnalytics } from "../utils/useAnalytics";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
  const { init, trackPageViewed } = useAnalytics();
  const isProduction = process.env.NODE_ENV === "production";
  useEffect(() => {
    if (isProduction) {
      init("UA-178040908-1");
      trackPageViewed();
      Router.events.on("routeChangeComplete", () => {
        trackPageViewed();
      });
    }
  }, []);
  return <Component {...pageProps} />;
}

export default MyApp;
