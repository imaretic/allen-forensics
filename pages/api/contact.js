const fetch = require("node-fetch");
const querystring = require("querystring");

import {
  validateName,
  validateEmail,
  validateMessage,
} from "../../utils/validator";

export default async (req, res) => {
  if (req.method === "POST") {
    const name = req.body.name;
    const email = req.body.email;
    const message = req.body.message;

    // Validate
    if (
      !validateName(name) ||
      !validateEmail(email) ||
      !validateMessage(message)
    ) {
      res.statusCode = 400;
      res.json({ message: "Bad request (invalid data)." });
      return;
    }

    // Send email
    const data = {
      name: name,
      email: email,
      recipientemail: "info@allenforensics.com",
      recipientsubject: "AllenForensics.com | New Message",
      message: message,
    };

    try {
      const response = await fetch("https://winner.agency/api/mailsender.php", {
        method: "post",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: querystring.stringify(data),
      });
      res.statusCode = response.status;
      if (response.status === 200) res.json({ message: "Email sent!" });
      else res.json({ message: "Bad request from mail sender!" });
    } catch (error) {
      console.log(error);
      res.statusCode = error.status;
      res.json({ message: "Error!" });
    }
  }
};
